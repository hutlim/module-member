(function($){
	$.entwine('ss', function($) {
		$('.ss-gridfield .action.login-link').entwine({
			onclick: function(e){
				window.open(this.prop('href'));
				e.preventDefault();
				return false;
			}
		});
	});
}(jQuery));
