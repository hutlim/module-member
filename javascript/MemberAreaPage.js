/**
 * File: MemberAreaPage.js
 */
(function($) {
	$.entwine('ss', function($){
		/**
		 * Class: .cms-edit-form #CanViewType
		 * 
		 * Toggle display of group dropdown in "access" tab,
		 * based on selection of radiobuttons.
		 */
		$('.cms-edit-form #RestrictType').entwine({
			// Constructor: onmatch
			onmatch: function() {
				// TODO Decouple
				var restrict_groups, restrict_users, restrict_network;
				var wrapper = this.find('.optionset :input').closest('.middleColumn').parent('div');
				if(this.attr('id') == 'RestrictType'){
					restrict_groups = $('#RestrictGroups');
					restrict_users = $('#RestrictUsers');
					restrict_network = $('#RestrictNetwork');
				}

				this.find('.optionset :input').bind('change', function(e) {
					if(e.target.value == 'OnlyTheseGroups') {
						wrapper.addClass('remove-splitter');
						restrict_groups['show']();
						restrict_users['hide']();
						restrict_network['hide']();
					}
					else if(e.target.value == 'OnlyTheseUsers') {
						wrapper.addClass('remove-splitter');
						restrict_groups['hide']();
						restrict_users['show']();
						restrict_network['hide']();
					}
					else if(e.target.value == 'OnlyTheseNetworks') {
						wrapper.addClass('remove-splitter');
						restrict_groups['hide']();
						restrict_users['hide']();
						restrict_network['show']();
					}
					else if(e.target.value == 'Any') {
						wrapper.addClass('remove-splitter');
						restrict_groups.find(':input').closest('.middleColumn').parent('div').addClass('remove-splitter');
						restrict_users.find(':input').closest('.middleColumn').parent('div').addClass('remove-splitter');
						restrict_groups['show']();
						restrict_users['show']();
						restrict_network['show']();
					}
					else {
						wrapper.removeClass('remove-splitter');
						restrict_groups['hide']();
						restrict_users['hide']();
						restrict_network['hide']();
					}
				});
		
				// initial state
				var currentVal = this.find('input[name=' + this.attr('id') + ']:checked').val();
				if(currentVal == 'OnlyTheseGroups') {
					wrapper.addClass('remove-splitter');
					restrict_groups['show']();
					restrict_users['hide']();
					restrict_network['hide']();
				}
				else if(currentVal == 'OnlyTheseUsers') {
					wrapper.addClass('remove-splitter');
					restrict_groups['hide']();
					restrict_users['show']();
					restrict_network['hide']();
				}
				else if(currentVal == 'OnlyTheseNetworks') {
					wrapper.addClass('remove-splitter');
					restrict_groups['hide']();
					restrict_users['hide']();
					restrict_network['show']();
				}
				else if(currentVal == 'Any') {
					wrapper.addClass('remove-splitter');
					restrict_groups.find(':input').closest('.middleColumn').parent('div').addClass('remove-splitter');
					restrict_users.find(':input').closest('.middleColumn').parent('div').addClass('remove-splitter');
					restrict_groups['show']();
					restrict_users['show']();
					restrict_network['show']();
				}
				else {
					wrapper.removeClass('remove-splitter');
					restrict_groups['hide']();
					restrict_users['hide']();
					restrict_network['hide']();
				}
				
				this._super();
			},
			onunmatch: function() {
				this._super();
			}
		});	
	});
}(jQuery));
