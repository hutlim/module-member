if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('zh', {
		'BankNameField.ERROR_LOADING' : '加载时发生错误，请重试。'
	});
}