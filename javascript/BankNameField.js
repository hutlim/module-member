(function($) {
	
	$(':text[rel~=bankname], select[rel~=bankname]').each(function(index) {
		var self = $(this), id = self.attr('id'), container = $(this).parent();
		if(self.data('country-field') != ''){
			var country_field = self.parents('form').find(':input[name="' + self.data('country-field') + '"]'), country = country_field.val(), url = self.data('url'), country_field_id = country_field.attr('id');
			if(country != '' && url != ''){
				$.ajax({
					url: url,
		          	dataType: 'html',
		          	data: {'country': country},
		          	beforeSend: function(xhr) {
		          		self.prop('disabled', true).addClass('loading').trigger('liszt:updated');
		          		self.next('.chzn-container').find('a span').html('Loading...').addClass('loading');
		          		country_field.addClass('load-bank').prop('disabled', true).trigger('liszt:updated');
		          	},
		          	spinner: false
		        })
				.done(function(data) {
					if(data != ''){
						var obj = container.clone();
						obj.find(':text[rel~=bankname], select[rel~=bankname]').remove();
						var message = obj.html();
						if(message != undefined){
							data += message;
						}
						container.html(data);
					}
					else{
						alert(ss.i18n._t('BankNameField.ERROR_LOADING', 'Error occur while loading, please try again.'));
					}
				})
				.always(function() {
					self.prop('disabled', false).removeClass('loading').trigger('liszt:updated');
					self.next('.chzn-container a span.loading').removeClass('loading');
					country_field.removeClass('load-bank');
					if(country_field.is('[class*=load-]') === false){
						country_field.prop('disabled', false).trigger('liszt:updated');
					}
		  		})
		  		.fail(function() {
    				alert(ss.i18n._t('BankNameField.ERROR_LOADING', 'Error occur while loading, please try again.'));
  				});
			}

			$('#' + country_field_id).live("change", function(e) {
				e.preventDefault();
				var self = $(this), country = country_field.val(), bank_field = $('#' + id), container = bank_field.parent(), url = bank_field.data('url');
				if(country != '' && url != ''){
					$.ajax({
						url: url,
			          	dataType: 'html',
			          	data: {'country': country},
			          	beforeSend: function(xhr) {
			          		bank_field.prop('disabled', true).addClass('loading').trigger('liszt:updated');
			          		bank_field.next('.chzn-container').find('a span').html('Loading...').addClass('loading');
			          		self.addClass('load-bank').prop('disabled', true).trigger('liszt:updated');
			          	},
			          	spinner: false
			        })
					.done(function(data) {
						if(data != ''){
							var obj = container.clone();
							obj.find(':text[rel~=bankname], select[rel~=bankname]').remove();
							var message = obj.html();
							if(message != undefined){
								data += message;
							}
							container.html(data);
						}
						else{
							alert(ss.i18n._t('BankNameField.ERROR_LOADING', 'Error occur while loading, please try again.'));
						}
					})
					.always(function() {
						bank_field.prop('disabled', false).removeClass('loading').trigger('liszt:updated');
						bank_field.next('.chzn-container a span.loading').removeClass('loading');
						self.removeClass('load-bank');
						if(self.is('[class*=load-]') === false){
							self.prop('disabled', false).trigger('liszt:updated');
						}
			  		})
			  		.fail(function() {
    					alert(ss.i18n._t('BankNameField.ERROR_LOADING', 'Error occur while loading, please try again.'));
  					});
				}
			});
		}
	});
})(jQuery);