<p><% _t('RegisterEmail.ss.CONGRATULATION', 'Congratulation') %> $Name,</p>

<p>
    <%t RegisterEmail.ss.MESSAGE "You are successfully registered and can look forward to enjoying privilege benefit." %><br />
    <a href="$MemberAreaLink" target="_blank"><%t RegisterEmail.ss.GO_TO_MEMBER_AREA "Go to member area here" %></a>
</p>