<?php

class MemberWidget extends Widget {
	private static $title = null; //don't show a title for this widget by default
	private static $cmsTitle = "Member Widget";
	private static $description = "Show member details";
}