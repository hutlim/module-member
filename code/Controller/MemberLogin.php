<?php

class MemberLogin extends Security {
    private static $allowed_actions = array(
    	'login',
        'LoginForm'
    );
	
	/**
	 * Show the "login" page
	 *
	 * @return string Returns the "login" page as HTML code.
	 */
	public function login() {
		Requirements::themedCSS('MemberLogin');

		// Get response handler
		$controller = $this->getResponseController(_t('MemberLogin.LOGIN', 'Member Log In'));

		// if the controller calls Director::redirect(), this will break early
		if(($response = $controller->getResponse()) && $response->isFinished()) return $response;

		$form = $this->LoginForm();
		$content = $form->forTemplate();
		
		if($message = $this->getLoginMessage()) {
			$customisedController = $controller->customise(array(
				"Content" => $message,
				"Form" => $content,
			));
		} else {
			$customisedController = $controller->customise(array(
				"Form" => $content,
			));
		}
		
		Session::clear('Security.Message');

		// custom processing
		return $customisedController->renderWith($this->getTemplatesFor('member_login'));
	}

	/**
	 * Get a link to a security action
	 *
	 * @param string $action Name of the action
	 * @return string Returns the link to the given action
	 */
	public function Link($action = null) {
		return Controller::join_links(Director::baseURL(), "member", $action);
	}
    
    function LoginForm(){
        return MyAuthenticator::get_login_form($this);
    }
	
	/**
	 * Prepare the controller for handling the response to this request
	 *
	 * @param string $title Title to use
	 * @return Controller
	 */
	protected function getResponseController($title) {
		if(!class_exists('SiteTree')) return $this;

		// Use sitetree pages to render the security page
		$tmpPage = new Page();
		$tmpPage->Title = $title;
		$tmpPage->URLSegment = "member";
		// Disable ID-based caching  of the log-in page by making it a random number
		$tmpPage->ID = -1 * rand(1,10000000);

		$controller = Page_Controller::create($tmpPage);
		$controller->setDataModel($this->model);
		$controller->init();
		return $controller;
	}
}
