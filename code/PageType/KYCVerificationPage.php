<?php
class KYCVerificationPage extends MemberPage {
	private static $singular_name = "KYC Verification Page";
    private static $plural_name = "KYC Verification Pages";
    private static $default_parent = 'MyAdminPage';

    private static $db = array();

    private static $has_one = array();

}

class KYCVerificationPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');
	
	public function init() {
        parent::init();
		
		$submit_msg = _t('KYCVerificationPage.SUBMIT_MSG', 'You\'re not able to change proof of document after submit. Are you sure you want to proceed submit?');
		$js = <<<JS
			(function($) {
				$('#Form_Form_action_doSaveKYC').on('click', function(e) {
					var submit_msg = "$submit_msg";
  					if (confirm(submit_msg)){
         				return true;
      				}
      				return false;
				});
			})(jQuery);
JS;
		Requirements::customScript($js, 'SubmitKYC');
    }

    function Form() {
		if($this->CurrentMember()->KYCStatus == 'Approved' || $this->CurrentMember()->KYCStatus == 'Process'){
			$fields = FieldList::create(
				ReadonlyField::create('KYCStatus', singleton('Member')->fieldLabel('KYCStatus'), $this->CurrentMember()->KYCStatus),
				ReadonlyField::create('ProofBank', singleton('Member')->fieldLabel('ProofBank'), $this->CurrentMember()->ProofBank()->Name),
				ReadonlyField::create('ProofAddress', singleton('Member')->fieldLabel('ProofAddress'), $this->CurrentMember()->ProofAddress()->Name),
				ReadonlyField::create('ProofPassport', singleton('Member')->fieldLabel('ProofPassport'), $this->CurrentMember()->ProofPassport()->Name)
			);
			
			$actions = FieldList::create();
			
			return Form::create($this, 'Form', $fields, $actions);
		}

		$fields = singleton('Member')->getKYCFields();
		if($this->CurrentMember()->ProofBank()->Name){
			$fields->dataFieldByName('ProofBank')->setDescription(sprintf('<p><strong class="text-success">%s</strong></p>%s', _t('KYCVerificationPage.UPLOADED_FILE', 'Uploaded File: {file}', '', array('file' => $this->CurrentMember()->ProofBank()->Name)), $fields->dataFieldByName('ProofBank')->getDescription()));
		}
		
		if($this->CurrentMember()->ProofAddress()->Name){
			$fields->dataFieldByName('ProofAddress')->setDescription(sprintf('<p><strong class="text-success">%s</strong></p>%s', _t('KYCVerificationPage.UPLOADED_FILE', 'Uploaded File: {file}', '', array('file' => $this->CurrentMember()->ProofAddress()->Name)), $fields->dataFieldByName('ProofAddress')->getDescription()));
		}
		
		if($this->CurrentMember()->ProofPassport()->Name){
			$fields->dataFieldByName('ProofPassport')->setDescription(sprintf('<p><strong class="text-success">%s</strong></p>%s', _t('KYCVerificationPage.UPLOADED_FILE', 'Uploaded File: {file}', '', array('file' => $this->CurrentMember()->ProofPassport()->Name)), $fields->dataFieldByName('ProofPassport')->getDescription()));
		}
		$fields->push(SecurityPinField::create('SecurityPin', _t('KYCVerificationPage.SECURITY_PIN', 'Security Pin')));

        $actions = FieldList::create(
            FormAction::create("doSaveKYC", _t('KYCVerificationPage.BUTTONSUBMIT', 'Submit'))
        );
        $validator = RequiredFields::create('ProofBank', 'ProofAddress', 'ProofPassport');

        return Form::create($this, 'Form', $fields, $actions, $validator)->loadDataFrom($this->CurrentMember());
    }
    
    function doSaveKYC($data, $form) {
        try {
            $member = $this->CurrentMember();
            $form->saveInto($member);
			$member->KYCStatus = 'Process';
            $member->write();
            $form->sessionMessage(_t('KYCVerificationPage.SUCCESS_SUBMIT_PROOF_DOCUMENT', 'Proof of document have been submitted successfully'), 'success');
        }
        catch(ValidationException $e) {
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->redirectBack();
    }
}
