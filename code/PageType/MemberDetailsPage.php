<?php
class MemberDetailsPage extends MemberPage {
    private static $default_parent = 'MyAdminPage';

    private static $db = array();

    private static $has_one = array();

}

class MemberDetailsPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');

    function Form() {
        $fields = singleton('Member')->getPersonalInfoFields();
		$fields->merge(singleton('Member')->getAddressFields(array('includeHeader' => false)));
		$fields->push(SecurityPinField::create('SecurityPin', _t('MemberDetailsPage.SECURITY_PIN', 'Security Pin')));
		
        $actions = FieldList::create(
            ResetFormAction::create("doReset", _t('MemberDetailsPage.BUTTONRESET', 'Reset')),
            FormAction::create("doSaveMember", _t('MemberDetailsPage.BUTTONUPDATE', 'Update'))
        );
        $validator = RequiredFields::create('Name', 'Email', 'Passport', 'Mobile', 'DOB');

        return Form::create($this, 'Form', $fields, $actions, $validator)->loadDataFrom($this->CurrentMember());
    }
    
    function doSaveMember($data, $form) {
        try {
            $member = $this->CurrentMember();
            $form->saveInto($member);
            $member->write();
            $form->sessionMessage(_t('MemberDetailsPage.SUCCESS_UPDATE_PERSONAL_DETAILS', 'Personal details have been updated successfully'), 'success');
        }
        catch(ValidationException $e) {
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->redirectBack();
    }
}
