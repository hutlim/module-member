<?php
class MemberOverviewPage extends MemberAreaPage {
    private static $db = array(
    	'IsDefaultPage' => 'Boolean',
    	'IsSecurePage' => 'Boolean'
	);

    private static $can_be_root = false;

    private static $default_parent = 'MemberAreaPage';
	
	private static $hide_ancestor = 'MemberOverviewPage';

    function getSettingsFields() {
        $fields = parent::getSettingsFields();
		if(!$this instanceof MemberPage){
        	$fields->addFieldToTab('Root.Settings', new CheckboxField('IsDefaultPage', _t('MemberOverviewPage.DEFAULT_PAGE', 'This is the member area default page.')));
		}
		if(Distributor::get_use_pin()){
			if(!$this->numChildren()){
				$fields->addFieldToTab('Root.Settings', new CheckboxField('IsSecurePage', _t('MemberOverviewPage.SECURE_PAGE', 'This is the member area secure page.')));
			}
		}
        return $fields;
    }
	
	function IsSecure(){
		if(!Distributor::get_use_pin()){
			return false;
		}

		return $this->IsSecurePage;
	}
}

class MemberOverviewPage_Controller extends MemberAreaPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
    	'secure',
        'SecureForm'
	);
	
	protected function handleAction($request, $action) {
		if($member = $this->CurrentMember() && Distributor::get_use_pin() && !Distributor::customLoginID()){
			if($action != 'secure' && $action != 'SecureForm' && !$this->AllowView() && $this->IsSecure() && !$this->redirectedTo()){
				$action = 'secure';
			}
		}
		
		return parent::handleAction($request, $action);
	}

	function secure(){
		if($this->AllowView()){
			return $this->redirect($this->Link());
		}
		
		return array(
			'Form' => $this->SecureForm()
		);
	}

	function SecureForm(){
		$fields = FieldList::create(
			SecurityPinField::create('SecurityPin', _t('MemberOverviewPage.SECURITY_PIN', 'Security Pin'))
		);
		
		$actions = FieldList::create(
            FormAction::create("doCheck", _t('MemberOverviewPage.BUTTONSUBMIT', 'Submit'))
        );
		
		$validator = RequiredFields::create('SecurityPin');

        return Form::create($this, 'SecureForm', $fields, $actions, $validator);
	}
	
	function doCheck($data, $form) {
        try {
            Session::set(sprintf('Secure.%s', $this->ClassName), true);
        }
        catch(ValidationException $e) {
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->redirectBack();
    }
	
	function AllowView(){
		return Session::get(sprintf('Secure.%s', $this->ClassName));
	}
}
