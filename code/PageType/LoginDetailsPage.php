<?php
class LoginDetailsPage extends MemberPage {
    private static $default_parent = 'MyAdminPage';

    private static $db = array();

    private static $has_one = array();

}

class LoginDetailsPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');

    public function init() {
        parent::init();
    }

    function Form() {
        $form = Form::create($this, 'Form', singleton('Member')->getLoginDetailsFields(), FieldList::create())->loadDataFrom($this->CurrentMember());
        $form->makeReadonly();
        return $form;
    }

}
