<?php
class UserPreferencesPage extends MemberPage {
    private static $default_parent = 'MyAdminPage';

    private static $db = array();

    private static $has_one = array();

}

class UserPreferencesPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');

    public function init() {
        parent::init();
    }

    function Form() {
        $fields = singleton('Member')->getUserPreferencesFields();
        $actions = FieldList::create(FormAction::create("doSave", _t('UserPreferencesPage.BUTTONUPDATE', 'Update')));

        return Form::create($this, 'Form', $fields, $actions)->loadDataFrom($this->CurrentMember());
    }

    function doSave($data, $form) {
        try {
            $member = $this->CurrentMember();
            $form->saveInto($member);
            $member->write();
            $form->sessionMessage(_t('UserPreferencesPage.SUCCESS_UPDATE_USER_PREFERENCES', 'User preferences have been updated successfully'), 'success');
        }
        catch(ValidationException $e) {
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->redirectBack();
    }

}
