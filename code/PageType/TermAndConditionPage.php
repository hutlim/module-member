<?php
class TermAndConditionPage extends MemberOverviewPage {

    private static $db = array();

    private static $has_one = array();
	
	private static $defaults = array(
		'ShowInMenus' => false,
		'ShowInSearch' => false
	);

}

class TermAndConditionPage_Controller extends MemberOverviewPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');

    public function init() {
        parent::init();
        
        if($this->CurrentMember()->AgreeTermAndCondition){
            return $this->redirect($this->MemberAreaLink);
        }
    }

    public function Form(){
        $fields = FieldList::create(
            HiddenField::create('AgreeTermAndCondition', 'AgreeTermAndCondition', 1)
        );
        
        $actions = FieldList::create(
            FormAction::create("doAgree", _t('TermAndConditionPage.BUTTONAGREED', 'I\'ve read and agree with {site_title} Terms and Conditions', '', array('site_title' => SiteConfig::current_site_config()->Title)))
        );

        return Form::create($this, 'Form', $fields, $actions);
    }
    
    public function doAgree($data, $form) {
        try {
            $member = $this->CurrentMember();
            $form->saveInto($member);
            $member->write();
        }
        catch(ValidationException $e) {
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->redirectBack();
    }
}
