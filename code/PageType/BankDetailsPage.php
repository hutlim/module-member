<?php
class BankDetailsPage extends MemberPage {
    private static $default_parent = 'MyAdminPage';

    private static $db = array();

    private static $has_one = array();

}

class BankDetailsPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');

    public function init() {
        parent::init();
		
		$confirm_msg = _t('BankDetailsPage.CONFIRM_MSG', 'You\'re not able to change bank details after confirm. Are you sure you want to proceed confirm?');
		$js = <<<JS
			(function($) {
				$('#Form_Form_action_doConfirm').on('click', function(e) {
					var confirm_msg = "$confirm_msg";
  					if (confirm(confirm_msg)){
         				return true;
      				}
      				return false;
				});
			})(jQuery);
JS;
		Requirements::customScript($js, 'ConfirmaBankDetails');
    }
    
    function Form() {
        $fields = singleton('Bank')->getBankDetailsFormFields();
        $actions = FieldList::create(
            FormAction::create("doSave", _t('BankDetailsPage.BUTTONUPDATE', 'Update'))
        );
		
		if(Distributor::get_use_kyc()){
			$actions->push(FormAction::create("doConfirm", _t('BankDetailsPage.BUTTONCONFIRM', 'Confirm')));
		}
		
        $validator = RequiredFields::create('BankName', 'Currency', 'Country', 'AccountName', 'AccountNumber');
        $bank = Bank::get()->find('MemberID', $this->CurrentMember()->ID);
		if($bank && $bank->Confirmed){
			$actions = FieldList::create();
			$form = Form::create($this, 'Form', $fields, $actions, $validator)->loadDataFrom($bank ? $bank : array());
			$form->Fields()->removeByName('BankSecurityPin');
			$form->makeReadonly();
			return $form;
		}
        return Form::create($this, 'Form', $fields, $actions, $validator)->loadDataFrom($bank ? $bank : array());
    }
    
    function doSave($data, $form) {
        try {
            $bank = Bank::get()->find('MemberID', $this->CurrentMember()->ID);
            if(!$bank){
                $bank = Bank::create()->setField('MemberID', $this->CurrentMember()->ID);
            }
            $form->saveInto($bank);
            $bank->write();
            $form->sessionMessage(_t('BankDetailsPage.SUCCESS_UPDATE_BANK_DETAILS', 'Bank details have been updated successfully'), 'success');
        }
        catch(ValidationException $e) {
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->redirectBack();
    }
	
	function doConfirm($data, $form) {
        try {
            $bank = Bank::get()->find('MemberID', $this->CurrentMember()->ID);
            if(!$bank){
                $bank = Bank::create()->setField('MemberID', $this->CurrentMember()->ID);
            }
			$form->saveInto($bank);
            $bank->Confirmed = 1;
            $bank->write();
            $form->sessionMessage(_t('BankDetailsPage.SUCCESS_CONFIRM_BANK_DETAILS', 'Bank details have been confirmed.'), 'success');
        }
        catch(ValidationException $e) {
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->redirectBack();
    }
}
