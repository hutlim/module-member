<?php
class ChangePinPage extends MemberPage {
    private static $default_parent = 'MyAdminPage';
    
    private static $db = array();

    private static $has_one = array();

    public function canCreate($member = null){
        if(Distributor::get_use_pin()){
			return parent::canCreate($member);
		}
		
		return false;
    }
	
	public function canEdit($member = null){
        if(Distributor::get_use_pin()){
			return parent::canEdit($member);
		}
		
        return false;
    }
	
	public function canView($member = null){
		if(Distributor::get_use_pin()){
			return parent::canView($member);
		}

        return false;
    }
	
	public function canDelete($member = null){
		if(Distributor::get_use_pin()){
			return parent::canDelete($member);
		}

        return false;
    }
}
class ChangePinPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array ('Form');

    public function init() {
        parent::init();
    }
    
    function Form(){        
        return MemberChangePinForm::create($this, 'Form');
    }
}
