<?php

class DashboardLatestMemberPanel extends DashboardPanel {
    private static $db = array('Count' => 'Int');

    private static $defaults = array('Count' => 10);

    private static $icon = "member/images/member-icon.png";

    public function getLabel() {
        return _t('DashboardLatestMemberPanel.LATEST_MEMBER', 'Latest Member');
    }

    public function getDescription() {
        return _t('DashboardLatestMemberPanel.SHOW_LATEST_MEMBER', 'Shows latest joined member.');
    }
    
    public function getSecondaryActions() {
        $actions = parent::getPrimaryActions();
        if(Permission::check('CMS_ACCESS_MemberAdmin') && (Permission::check('EDIT_Distributor') || Permission::check('VIEW_Distributor'))){
            $link = Controller::join_links(MemberAdmin::create()->Link('Member'));
			$actions->push(DashboardPanelAjaxAction::create(
                Controller::join_links($link, sprintf('?q[JoinedYear]=%s', date('Y'))), 
                _t('DashboardLatestMemberPanel.BUTTONVIEWTHISYEARMEMBERS', 'View this year members')   
            ));
			$actions->push(DashboardPanelAjaxAction::create(
                $link, 
                _t('DashboardLatestMemberPanel.BUTTONVIEWALLMEMBERS', 'View all members')   
            ));
        }
        return $actions;
    }

    public function getConfiguration() {
        $fields = parent::getConfiguration();
        $fields->push(TextField::create("Count", _t('DashboardLatestMemberPanel.NUMBER_OF_MEMBER_SHOW', 'Number of member to show')));
        return $fields;
    }

    protected function EditLink($id) {
        if(Permission::check('CMS_ACCESS_MemberAdmin') && (Permission::check('EDIT_Distributor') || Permission::check('VIEW_Distributor'))){
            $link = Controller::join_links(MemberAdmin::create()->Link('Member'), 'EditForm', 'field', 'Member', 'item', $id, 'edit');
        }
        else{
            $link = 'javascript:void(0)';
        }
        return $link;
    }

    public function Members() {
        $members = Member::get()
		->filter('IsDistributor', 1)
        ->sort('JoinedDate', 'DESC')
        ->limit($this->Count);
        $set = ArrayList::create(array());
        foreach($members as $r) {
            $set->push(ArrayData::create(array(
                'EditLink' => $this->EditLink($r->ID),
                'Title' => _t('DashboardLatestMemberPanel.MEMBER_JOINED_ON', '{username} ({name}) has joined on {joined}', '', array('username' => $r->Username, 'name' => $r->Name, 'joined' => $r->obj('JoinedDate')->Nice()))
            )));
        }
        return $set;
    }

	public function TotalMemberJoined(){
		return $members = Member::get()->filter('IsDistributor', 1)->count();
	}
	
	public function MemberJoinedThisYear(){
		return $members = Member::get()->filter('IsDistributor', 1)->filter('JoinedDate:YearMatch', date('Y'))->count();
	}
}
