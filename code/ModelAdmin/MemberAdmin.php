<?php
/**
 * Member administration interface, based on ModelAdmin
 * @package site
 */
class MemberAdmin extends GeneralModelAdmin {

    private static $url_segment = 'member';
    private static $menu_title = 'Member';
    private static $menu_icon = 'member/images/member-icon.png';
	public $showImportForm = false;

    private static $managed_models = array(
    	'Member'
    );
	
	public function getEditForm($id = null, $fields = null) {
        $list = $this->getList();
        if(ClassInfo::exists('GridFieldExportToExcelButton')){
        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
		}
		else{
			$exportButton = new GridFieldExportButton('buttons-after-left');
		}
        $exportButton->setExportColumns($this->getExportFields());
        
		if($this->modelClass == 'Member'){
			$listField = GridField::create(
	            $this->sanitiseClassName($this->modelClass),
	            false,
	            $list,
	            $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
					->removeComponentsByType('GridFieldDetailForm')
	                ->removeComponentsByType('GridFieldFilterHeader')
	                ->addComponents(new MemberGridFieldDetailForm(), new GridFieldLoginButton(), new MemberGridFieldStatusAction(), new GridFieldEmailButton(), new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
	        );
			
			if(Permission::check('EDIT_Distributor')){
				$fieldConfig->addComponent($bulkButton = new GridFieldBulkAction(), 'GridFieldSortableHeader');
				
				$bulkButton
				->addBulkAction('active', _t('MemberAdmin.ACTIVE', 'Active'), 'GridFieldBulkMemberHandler')
				->addBulkAction('suspend', _t('MemberAdmin.SUSPEND', 'Suspend'), 'GridFieldBulkMemberHandler', array('icon' => 'cross-circle'));
			}
		
		}
		else{
	        $listField = GridField::create(
	            $this->sanitiseClassName($this->modelClass),
	            false,
	            $list,
	            $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
	                ->removeComponentsByType('GridFieldFilterHeader')
	                ->addComponents(new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
	        );
		}
        
		// Validation
        if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
            $detailValidator = singleton($this->modelClass)->getCMSValidator();
			if($this->modelClass == 'Member'){
				$listField->getConfig()->getComponentByType('MemberGridFieldDetailForm')->setValidator($detailValidator);
			}
			else{
            	$listField->getConfig()->getComponentByType('GridFieldDetailForm')->setValidator($detailValidator);
			}
        }
		
		$note = new LiteralField('MembersCautionText',
			sprintf('<p class="caution-remove"><strong>%s</strong></p>',
				_t(
					'MemberAdmin.MemberListCaution', 
					'Caution: Removing members from this list will remove them from all groups and the'
						. ' database'
				)
			)
		);

        $form = CMSForm::create( 
            $this,
            'EditForm',
            new FieldList($listField, $note),
            new FieldList()
        )->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
        $form->addExtraClass('cms-edit-form cms-panel-padded center');
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
        $editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
        $form->setFormAction($editFormAction);
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');

        $this->extend('updateEditForm', $form);
        
        return $form;
    }

	public function getSearchContext() {
		if($this->modelClass == 'Member'){
			$context = singleton($this->modelClass)->getDefaultSearchContext();
			$context->addField(TextField::create('JoinedYear', _t('MemberAdmin.JOINED_YEAR', 'Joined Year')));
		}
		
		// Namespace fields, for easier detection if a search is present
		foreach($context->getFields() as $field) $field->setName(sprintf('q[%s]', $field->getName()));
		foreach($context->getFilters() as $filter) $filter->setFullName(sprintf('q[%s]', $filter->getFullName()));

		$this->extend('updateSearchContext', $context);

		return $context;
	}

	public function getList() {
		$context = $this->getSearchContext();
		$params = $this->request->requestVar('q');
		$list = $context->getResults($params);
		
		if($this->modelClass == 'Member'){
			$list = $list->filter('IsDistributor', 1);
			if(isset($params['JoinedYear']) && $params['JoinedYear']){
				$filter = YearMatchFilter::create('JoinedDate');
				$filter->setModel($this->modelClass);
				$filter->setValue($params['JoinedYear']);
				if(!$filter->isEmpty()) {
					$list = $list->alterDataQuery(array($filter, 'apply'));
				}
			}
		}

		$this->extend('updateList', $list);

		return $list;
	}
}
?>