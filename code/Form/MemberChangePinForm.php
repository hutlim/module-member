<?php
/**
 * Standard Member Change Pin Form
 */
class MemberChangePinForm extends Form {

    /**
     * Constructor
     *
     * @param Controller $controller The parent controller, necessary to
     *                               create the appropriate form action tag.
     * @param string $name The method on the controller that will return this
     *                     form object.
     * @param FieldList|FormField $fields All of the fields in the form - a
     *                                   {@link FieldList} of {@link FormField}
     *                                   objects.
     * @param FieldList|FormAction $actions All of the action buttons in the
     *                                     form - a {@link FieldList} of
     */
    function __construct($controller, $name, $fields = null, $actions = null, $validator = null) {
        if(!$fields) {
            $fields = new FieldList();
            if($controller->CurrentMember() && $controller->CurrentMember()->Pin){
                $fields->push(PasswordField::create("OldPin",_t('MemberChangePinForm.YOUROLDPIN', "Your old pin")));
            }
            
            $fields->push(PasswordField::create("NewPin1", _t('MemberChangePinForm.NEWPIN', "New Pin")));
            $fields->push(PasswordField::create("NewPin2", _t('MemberChangePinForm.CONFIRMNEWPIN', "Confirm New Pin")));
        }
        if(!$actions) {
        	$btn_text = ($controller->CurrentMember() && $controller->CurrentMember()->Pin) ? _t('MemberChangePinForm.BUTTONCHANGEPIN', "Change Pin") : _t('MemberChangePinForm.BUTTONSETNEWPIN', 'Set New Pin');
            $actions = FieldList::create(FormAction::create("doChangePin", $btn_text));
        }

        parent::__construct($controller, $name, $fields, $actions, $validator);
    }

    /**
     * Change the pin
     *
     * @param array $data The user submitted data
     */
    function doChangePin(array $data, $form) {
        try {
        	$success_text = $this->controller->CurrentMember()->Pin ? _t('MemberChangePinForm.CHANGEPINSUCCESSFULLY', "Your pin has been changed successfully") : _t('MemberChangePinForm.SETPINSUCCESSFULLY', "Your pin has been set successfully");
            $isValid = $this->controller->CurrentMember()->changePin($data['NewPin1']);
            if($isValid->valid()) {
                $form->sessionMessage($success_text, 'success');
            } else {
                $form->sessionMessage(
                    _t(
                        'MemberChangePinForm.INVALIDNEWPIN', 
                        "We couldn't accept that pin: {pin}",
                        array('pin' => nl2br("\n".$isValid->starredList()))
                    ), 
                    'error'
                );
            }
        }
        catch(ValidationException $e) {
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->controller->redirectBack();
    }

    function validate(){
        $valid = parent::validate();
        
        $data = $this->getData();
        if($this->controller->CurrentMember()->Pin && (empty($data['OldPin']) || !$this->controller->CurrentMember()->checkPin($data['OldPin'])->valid())) {
            $this->addErrorMessage(
                'OldPin',
                _t('MemberChangePinForm.ERRORPINNOTMATCH', "Your current pin does not match, please try again"),
                'validate'
            );
            Session::set("FormInfo.{$this->FormName()}.data", $data);
            $valid = false;
        }
        else if(empty($data['NewPin1'])) {
            $this->addErrorMessage('NewPin1', _t('MemberChangePinForm.EMPTYNEWPIN', "The new pin can't be empty, please try again"), 'validate');
            Session::set("FormInfo.{$this->FormName()}.data", $data);
            $valid = false;
        }
        else if($data['NewPin1'] != $data['NewPin2']) {
            $this->addErrorMessage('NewPin2', _t('MemberChangePinForm.ERRORNEWPIN', "You have entered your new pin differently, try again"), 'validate');
            Session::set("FormInfo.{$this->FormName()}.data", $data);
            $valid = false;
        }
        
        return $valid;
    }
}
?>