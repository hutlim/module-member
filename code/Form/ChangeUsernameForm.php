<?php
/**
 * Standard Change Username Form
 */
class ChangeUsernameForm extends Form {

    /**
     * Constructor
     *
     * @param Controller $controller The parent controller, necessary to
     *                               create the appropriate form action tag.
     * @param string $name The method on the controller that will return this
     *                     form object.
     * @param FieldList|FormField $fields All of the fields in the form - a
     *                                   {@link FieldList} of {@link FormField}
     *                                   objects.
     * @param FieldList|FormAction $actions All of the action buttons in the
     *                                     form - a {@link FieldList} of
     */
    function __construct($controller, $name, $fields = null, $actions = null, $validator = null) {
        if(!$fields) {
            $fields = FieldList::create(
                $current_username_field = ReadonlyField::create('CurrentUsername', _t('ChangeUsernameForm.CURRENT_USERNAME', 'Current Username'), $controller->CurrentMember()->Username),
                UniqueUsernameField::create('ToUsername', _t('ChangeUsernameForm.NEW_USERNAME', 'The username you wish to change')),
				SecurityPinField::create('SecurityPin', _t('ChangeUsernameForm.SECURITY_PIN', 'Security Pin'))
            );
			
			$current_username_field->setIncludeHiddenField(true);
        }
        if(!$actions) {
            $actions = FieldList::create(FormAction::create("doChangeUsername", _t('ChangeUsernameForm.BUTTONCHANGEUSERNAME', 'Change Username')));
        }

        if(!$validator) {
            $validator = RequiredFields::create('ToUsername');
        }

        parent::__construct($controller, $name, $fields, $actions, $validator);
    }

    /**
     * Change the username
     *
     * @param array $data The user submitted data
     */
    function doChangeUsername(array $data, $form) {
        try {
            $this->controller->CurrentMember()->setField('Username', $data['ToUsername'])->write();
            $form->sessionMessage(_t('ChangeUsernameForm.SUCCESS_CHANGE_USERNAME', 'Your username have been changed successfully'), 'success');
        }
        catch(ValidationException $e) {
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->controller->redirectBack();
    }

    function validate(){
        $valid = parent::validate();
        $data = $this->getData();
        $username = $data['ToUsername'];
        if(Member::get()->find('Username', $username) || SiteTree::get_by_link($username) || strtolower($username) == 'home') {
            $this->addErrorMessage('ToUsername', _t('ChangeUsernameForm.USERNAME_NOT_UNIQUE', "The username is already used by other member"), 'validate');
            Session::set("FormInfo.{$this->FormName()}.data", $data);
            $valid = false;
        }
        
        return $valid;
    }
}
?>