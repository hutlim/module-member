<?php
/**
 * Standard Member Change Password Form
 */
class MemberChangePasswordForm extends Form {

    /**
     * Constructor
     *
     * @param Controller $controller The parent controller, necessary to
     *                               create the appropriate form action tag.
     * @param string $name The method on the controller that will return this
     *                     form object.
     * @param FieldList|FormField $fields All of the fields in the form - a
     *                                   {@link FieldList} of {@link FormField}
     *                                   objects.
     * @param FieldList|FormAction $actions All of the action buttons in the
     *                                     form - a {@link FieldList} of
     */
    function __construct($controller, $name, $fields = null, $actions = null, $validator = null) {
        if(!$fields) {
            $fields = new FieldList();
            if($controller->CurrentMember()->Password){
                $fields->push(PasswordField::create("OldPassword",_t('MemberChangePasswordForm.YOUROLDPASSWORD', "Your old password")));
            }
            
            $fields->push($password = PasswordField::create("NewPassword1", _t('MemberChangePasswordForm.NEWPASSWORD', "New Password")));
            $fields->push(PasswordField::create("NewPassword2", _t('MemberChangePasswordForm.CONFIRMNEWPASSWORD', "Confirm New Password")));
			$fields->push(SecurityPinField::create('SecurityPin', _t('MemberChangePasswordForm.SECURITY_PIN', 'Security Pin')));
			
			$password->requireStrongPassword = true;
        }
        if(!$actions) {
            $actions = FieldList::create(FormAction::create("doChangePassword", _t('MemberChangePasswordForm.BUTTONCHANGEPASSWORD', "Change Password")));
        }

        parent::__construct($controller, $name, $fields, $actions, $validator);
    }

    /**
     * Change the password
     *
     * @param array $data The user submitted data
     */
    function doChangePassword(array $data, $form) {
        try {
            $isValid = $this->controller->CurrentMember()->changePassword($data['NewPassword1']);
            if($isValid->valid()) {
                if(!Distributor::customLoginID()){
                    $this->controller->CurrentMember()->logIn();
                }
                $form->sessionMessage(_t('MemberChangePasswordForm.CHANGEPASSWORDSUCCESSFULLY', "Your password has been changed successfully"), 'success');
            } else {
                $form->sessionMessage(
                    _t(
                        'MemberChangePasswordForm.INVALIDNEWPASSWORD', 
                        "We couldn't accept that password: {password}",
                        array('password' => nl2br("\n".$isValid->starredList()))
                    ), 
                    'error'
                );
            }
        }
        catch(ValidationException $e) {
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->controller->redirectBack();
    }

    function validate(){
        $valid = parent::validate();
        
        $data = $this->getData();
        if(empty($data['OldPassword']) || !$this->controller->CurrentMember()->checkPassword($data['OldPassword'])->valid()) {
            $this->addErrorMessage(
                'OldPassword',
                _t('MemberChangePasswordForm.ERRORPASSWORDNOTMATCH', "Your current password does not match, please try again"),
                'validate'
            );
            Session::set("FormInfo.{$this->FormName()}.data", $data);
            $valid = false;
        }
        else if(empty($data['NewPassword1'])) {
            $this->addErrorMessage('NewPassword1', _t('MemberChangePasswordForm.EMPTYNEWPASSWORD', "The new password can't be empty, please try again"), 'validate');
            Session::set("FormInfo.{$this->FormName()}.data", $data);
            $valid = false;
        }
        else if($data['NewPassword1'] != $data['NewPassword2']) {
            $this->addErrorMessage('NewPassword2', _t('MemberChangePasswordForm.ERRORNEWPASSWORD', "You have entered your new password differently, try again"), 'validate');
            Session::set("FormInfo.{$this->FormName()}.data", $data);
            $valid = false;
        }
        
        return $valid;
    }
}
?>