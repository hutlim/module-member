<?php
/**
 * @package memebr
 */
class GridFieldLoginButton implements GridField_ColumnProvider {
	public function augmentColumns($field, &$cols) {
		if(!in_array('Actions', $cols)) $cols[] = 'Actions';
	}

	public function getColumnsHandled($field) {
		return array('Actions');
	}

	public function getColumnContent($field, $record, $col) {
		if($record->canLoginView()) {
			Requirements::javascript('member/javascript/GridFieldLoginButton.js');
		    Requirements::css('member/css/GridFieldLoginButton.css');
			$data = new ArrayData(array(
				'Link' => Controller::join_links($field->Link('item'), $record->ID, 'login')
			));
			return $data->renderWith('GridFieldLoginButton');
		}
	}

	public function getColumnAttributes($field, $record, $col) {
		return array('class' => 'col-buttons');
	}

	public function getColumnMetadata($gridField, $col) {
		return array('title' => null);
	}
}
