<?php
class BankNameField extends TextField {
	private static $allowed_actions = array(
        'get_bank_list'
    );
	
	/**
     * The url to use as the check bank list
     * @var string
     */
    protected $bankListURL;
	
	protected $countryField;
	
	protected $country;

	function __construct($name, $title = null, $value = '', $maxLength = null, $form = null) {
		Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
		Requirements::add_i18n_javascript('member/javascript/lang');
        Requirements::javascript('member/javascript/BankNameField.min.js');
		Requirements::css('member/css/BankNameField.css');
		parent::__construct($name, $title, $value, $maxLength, $form);
	}
	
	function Field($properties = array()) {
		if($this->getCountryField()){
			if($this->getCountry()){
				if(singleton('BankList')->mapByCountry($this->getCountry())->count()){
					return singleton('BankList')->getDropdownFieldByCountry($this->getCountry(), $this->getName(), $this->Title())->setValue($this->Value())->setForm($this->getForm())->setAttribute('rel', 'bankname')->setAttribute('data-url', $this->getBankListURL())->setAttribute('data-country-field', $this->getCountryField())->Field();
				}
			}
			else{
				$this->setDisabled(true);
			}
		}

        return parent::Field($properties);
    }
	
	function getAttributes() {
        return array_merge(
            parent::getAttributes(), array(
            	'rel' => 'bankname',
                'data-url' => $this->getBankListURL(),
                'data-country-field' => $this->getCountryField()
            )
        );
    }
	
	function Type() {
		return 'bankname text dropdown';
	}
	
	public function performReadonlyTransformation() {
		$field = $this->castedCopy('ReadonlyBankNameField');
		$field->setReadonly(true);
		
		return $field;
	}
	
	/**
     * Set the URL used to get bank list.
     * 
     * @param string $URL The URL used for get bank list.
     */
    function setBankListURL($url) {
        $this->bankListURL = $url;
    }
	
	/**
     * Get the URL used to get bank list.
     *  
     * @return The URL used for get bank list.
     */
    function getBankListURL() {

        if (!empty($this->bankListURL)){
            return $this->bankListURL;
		}

        // Attempt to link back to itself
        return $this->Link('get_bank_list');
    }
	
	/**
     * Set the country field used for get bank list.
     * 
     * @param string $country_field The country field used for get bank list.
     */
    function setCountryField($country_field) {
        $this->countryField = $country_field;
		return $this;
    }
	
	/**
     * Get the country field used to get bank list.
     *  
     * @return The URL used for get bank list.
     */
    function getCountryField() {
        return $this->countryField;
    }
	
	/**
     * Set the country used for get bank list.
     * 
     * @param string $country The country used for get bank list.
     */
    function setCountry($country) {
        $this->country = $country;
		return $this;
    }
	
	/**
     * Get the country used to get bank list.
     *  
     * @return string $country The country used for get bank list.
     */
    function getCountry() {
        return $this->country;
    }

	/**
     * Handle a request for bank list checking.
     * 
     * @param HTTPRequest $request The request to handle.
     * @return bank list result
     */
    function get_bank_list(HTTPRequest $request) {
		$this->setCountry($request->getVar('country'));
		return $this->Field();
    }
}

class ReadonlyBankNameField extends ReadonlyField {
	public function Field($properties = array()) {
		// Include a hidden field in the HTML
		$inputValue = $attrValue = $this->Value();
		$banks = singleton('BankList')->map()->toArray();
		if(isset($banks[$inputValue])){
			$attrValue = $banks[$inputValue];
		}
		if($this->includeHiddenField && $this->readonly) {
			return "<span class=\"readonly\" id=\"" . $this->id() .
			"\">$attrValue</span><input type=\"hidden\" name=\"" . $this->name .
			"\" value=\"" . $inputValue . "\" />";
		} else {
			if(isset($banks[$inputValue])){
				$inputValue = $banks[$inputValue];
			}
			return "<span class=\"readonly\" id=\"" . $this->id() .
			"\">$attrValue</span>";
		}
	}
}
?>
