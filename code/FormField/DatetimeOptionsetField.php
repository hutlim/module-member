<?php

/**
 * DatetimeOptionsetField for Distributor
 *
 * @package member
 */
class DatetimeOptionsetField extends OptionsetField {

    function Field($properties = array()) {
        Requirements::css(SAPPHIRE_DIR . '/css/MemberDatetimeOptionsetField.css');
        Requirements::javascript(THIRDPARTY_DIR . '/thirdparty/jquery/jquery.js');
        Requirements::javascript(SAPPHIRE_DIR . '/javascript/MemberDatetimeOptionsetField.js');

        $options = '';
        $odd = 0;
        $source = $this->getSource();

        foreach($source as $key => $value) {
            // convert the ID to an HTML safe value (dots are not replaced, as
            // they are valid in an ID attribute)
            $itemID = $this->id() . '_' . preg_replace('/[^\.a-zA-Z0-9\-\_]/', '_', $key);
            if($key == $this->value) {
                $useValue = false;
                $checked = " checked=\"checked\"";
            }
            else {
                $checked = "";
            }

            $odd = ($odd + 1) % 2;
            $extraClass = $odd ? "odd" : "even";
            $extraClass .= " val" . preg_replace('/[^a-zA-Z0-9\-\_]/', '_', $key);
            $disabled = ($this->disabled || in_array($key, $this->disabledItems)) ? "disabled=\"disabled\"" : "";
            $ATT_key = Convert::raw2att($key);

            $options .= "<li class=\"" . $extraClass . "\"><input id=\"$itemID\" name=\"$this->name\" type=\"radio\" value=\"$key\"$checked $disabled class=\"radio\" /> <label title=\"$ATT_key\" for=\"$itemID\">$value</label></li>\n";
        }

        $id = $this->id();
        return "<ul id=\"$id\" class=\"optionset {$this->extraClass()}\">\n$options</ul>\n";
    }

    function setValue($value) {
        if($value == '__custom__') {
            $value = isset($_REQUEST[$this->name . '_custom']) ? $_REQUEST[$this->name . '_custom'] : null;
        }
        if($value) {
            parent::setValue($value);
        }
        return $this;
    }

    function validate($validator) {
        $value = isset($_POST[$this->name . '_custom']) ? $_POST[$this->name . '_custom'] : null;
        if( !$value)
            return true;
        // no custom value, don't validate

        require_once 'Zend/Date.php';
        $date = Zend_Date::now()->toString($value);
        $valid = Zend_Date::isDate($date, $value);
        if($valid) {
            return true;
        }
        else {
            if($validator) {
                $validator->validationError($this->name, _t('DatetimeOptionsetField.DATEFORMATBAD', "Date format is invalid"), "validation", false);
            }
            return false;
        }
    }

}
?>