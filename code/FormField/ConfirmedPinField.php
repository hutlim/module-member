<?php

/**
 * Two masked input fields, checks for matching pin.
 * 
 * @package forms
 * @subpackage fields-formattedinput
 */
class ConfirmedPinField extends FormField {
	
	/**
	 * Minimum character length of the pin.
	 *
	 * @var int
	 */
	public $minLength = null;
	
	/**
	 * Maximum character length of the pin.
	 *
	 * @var int
	 */
	public $maxLength = null;
	
	/**
	 * Enforces at least one digit and one alphanumeric
	 * character (in addition to {$minLength} and {$maxLength}
	 *
	 * @var boolean
	 */
	public $requireStrongPin = false;
	
	/**
	 * Allow empty fields in serverside validation
	 *
	 * @var boolean
	 */
	public $canBeEmpty = false;

	/**
	 * A place to temporarly store the confirm pin value
	 * @var string
	 */
	protected $confirmValue;
	
	/**
	 * Child fields (_Pin, _ConfirmPin)
	 * 
	 * @var FieldList
	 */
	public $children;
	
	/**
	 * @param string $name
	 * @param string $title
	 * @param mixed $value
	 * @param Form $form
	 * @param string $titleConfirmField Alternate title (not localizeable)
	 */
	public function __construct($name, $title = null, $value = "", $form = null, $titleConfirmField = null) {

		// naming with underscores to prevent values from actually being saved somewhere
		$this->children = new FieldList(
			new PasswordField(
				"{$name}[_Pin]", 
				(isset($title)) ? $title : _t('ConfirmedPinField.PIN', 'Security Pin')
			),
			new PasswordField(
				"{$name}[_ConfirmPin]",
				(isset($titleConfirmField)) ? $titleConfirmField : _t('ConfirmedPinField.CONFIRMPIN', 'Confirm Security Pin')
			)
		);

		// disable auto complete
		foreach($this->children as $child) {
			$child->setAttribute('autocomplete', 'off');
		}
		
		// we have labels for the subfields
		$title = false;
		
		parent::__construct($name, $title, null, $form);
		$this->setValue($value);
	}
	
	/**
	 * @param array $properties
	 *
	 * @return string
	 */
	public function Field($properties = array()) {
		$content = '';

		foreach($this->children as $field) {
			$field->setDisabled($this->isDisabled()); 
			$field->setReadonly($this->isReadonly());

			if(count($this->attributes)) {
				foreach($this->attributes as $name => $value) {
					$field->setAttribute($name, $value);
				}
			}

			$content .= $field->FieldHolder();
		}
		
		return $content;
	}
	
	/**
	 * Can be empty is a flag that turns on / off empty field checking.
	 *
	 * For example, set this to false (the default) when creating a user account,
	 * and true when displaying on an edit form.
	 *
	 * @param boolean $value
	 * 
	 * @return ConfirmedPinField
	 */
	public function setCanBeEmpty($value) {
		$this->canBeEmpty = (bool)$value;

		return $this;
	}
	
	/**
	 * @param string $title
	 *
	 * @return ConfirmedPinField
	 */
	public function setRightTitle($title) {
		foreach($this->children as $field) {
			$field->setRightTitle($title);
		}

		return $this;
	}
	
	/**
	 * @param array $titles 2 entry array with the customized title for each 
	 *						of the 2 children.
	 *
	 * @return ConfirmedPinField
	 */
	public function setChildrenTitles($titles) {
		if(is_array($titles) && count($titles) == 2) {
			foreach($this->children as $field) {
				if(isset($titles[0])) {
					$field->setTitle($titles[0]);

					array_shift($titles);		
				}
			}
		}

		return $this;
	}
	
	/**
	 * Value is sometimes an array, and sometimes a single value, so we need 
	 * to handle both cases.
	 *
	 * @param mixed $value
	 *
	 * @return ConfirmedPinField
	 */
	public function setValue($value, $data = null) {
		// If $data is a DataObject, don't use the value, since it's a hashed value
		if ($data && $data instanceof DataObject) $value = '';
		
		//store this for later
		$oldValue = $this->value;

		if(is_array($value)) {
			$this->value = $value['_Pin'];
			$this->confirmValue = $value['_ConfirmPin'];

		} else {
			if($value || (!$value && $this->canBeEmpty)) {
				$this->value = $value;
			}
		}

		//looking up field by name is expensive, so lets check it needs to change
		if ($oldValue != $this->value) {
			$this->children->fieldByName($this->getName() . '[_Pin]')
				->setValue($this->value);

			$this->children->fieldByName($this->getName() . '[_ConfirmPin]')
				->setValue($this->value);
		}

		return $this;
	}

	/**
	 * Update the names of the child fields when updating name of field.
	 * 
	 * @param string $name new name to give to the field.
	 */
	public function setName($name) {
		$this->children->fieldByName($this->getName() . '[_Pin]')
				->setName($name . '[_Pin]');
		$this->children->fieldByName($this->getName() . '[_ConfirmPin]')
				->setName($name . '[_ConfirmPin]');

		return parent::setName($name);
	}
	
	/**
	 * @param Validator $validator
	 *
	 * @return boolean
	 */
	public function validate($validator) {
		$name = $this->name;
		
		$pinField = $this->children->fieldByName($name.'[_Pin]');
		$pinConfirmField = $this->children->fieldByName($name.'[_ConfirmPin]');
		$pinField->setValue($this->value);
		$pinConfirmField->setValue($this->confirmValue);

		$value = $pinField->Value();
		
		// both pin-fields should be the same
		if($value != $pinConfirmField->Value()) {
			$validator->validationError(
				$name, 
				_t('ConfirmedPinField.VALIDATIONPINDONTMATCH',"Security Pin don't match"),
				"validation", 
				false
			);

			return false;
		}

		if(!$this->canBeEmpty) {
			// both pin-fields shouldn't be empty
			if(!$value || !$pinConfirmField->Value()) {
				$validator->validationError(
					$name, 
					_t('ConfirmedPinField.VALIDATIONPINNOTEMPTY', "Security Pin can't be empty"),
					"validation", 
					false
				);
	
				return false;
			}
		}
			
		// lengths
		if(($this->minLength || $this->maxLength)) {
			if($this->minLength && $this->maxLength) {
				$limit = "{{$this->minLength},{$this->maxLength}}";
				$errorMsg = _t(
					'ConfirmedPinField.BETWEEN', 
					'Security Pin must be {min} to {max} characters long.', 
					array('min' => $this->minLength, 'max' => $this->maxLength)
				);
			} elseif($this->minLength) {
				$limit = "{{$this->minLength}}.*";
				$errorMsg = _t(
					'ConfirmedPinField.ATLEAST', 
					'Security Pin must be at least {min} characters long.', 
					array('min' => $this->minLength)
				);
			} elseif($this->maxLength) {
				$limit = "{0,{$this->maxLength}}";
				$errorMsg = _t(
					'ConfirmedPinField.MAXIMUM', 
					'Security Pin must be at most {max} characters long.', 
					array('max' => $this->maxLength)
				);
			}
			$limitRegex = '/^.' . $limit . '$/';
			if(!empty($value) && !preg_match($limitRegex,$value)) {
				$validator->validationError(
					$name,
					$errorMsg,
					"validation", 
					false
				);
			}
		}
		
		if($this->requireStrongPin) {
			if(!preg_match('/^(([a-zA-Z]+\d+)|(\d+[a-zA-Z]+))[a-zA-Z0-9]*$/',$value)) {
				$validator->validationError(
					$name,
					_t('ConfirmedPinField.VALIDATIONSTRONGPIN',
						"Security Pin must have at least one digit and one alphanumeric character"), 
					"validation", 
					false
				);

				return false;
			}
		}

		return true;
	}
	
	/**
	 * Only save if field was shown on the client, and is not empty.
	 *
	 * @param DataObjectInterface $record
	 *
	 * @return boolean
	 */
	public function saveInto(DataObjectInterface $record) {
		if($this->value) {
			parent::saveInto($record);
		}
	}
	
	/**
	 * Makes a read only field with some stars in it to replace the pin
	 *
	 * @return ReadonlyField
	 */
	public function performReadonlyTransformation() {
		$field = $this->castedCopy('ReadonlyField')
			->setTitle($this->title ? $this->title : _t('ConfirmedPinField.PIN', 'Security Pin'))
			->setValue('*****');

		return $field;
	}
}
