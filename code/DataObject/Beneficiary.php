<?php
/**
 * @package member
 */
class Beneficiary extends DataObject {
    private static $singular_name = "Beneficiary";
    private static $plural_name = "Beneficiaries";
    
    private static $db = array(
        'Name' => 'Varchar(100)',
        'Relationship' => 'Varchar',
        'Passport' => 'Varchar',
        'Contact' => 'Varchar'
    );

    private static $has_one = array('Member' => 'Member');

    private static $searchable_fields = array(
        'Member.Username',
        'Member.FirstName',
        'Member.Surname',
        'Name',
        'Relationship',
        'Passport',
        'Contact'
    );

    private static $summary_fields = array(
        'Member.Username',
        'Member.Name',
        'Name',
        'Relationship',
        'Passport',
        'Contact'
    );

	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['Name'] = _t('Beneficiary.NAME', 'Name');
		$labels['Relationship'] = _t('Beneficiary.RELATIONSHIP', 'Relationship');
		$labels['Passport'] = _t('Beneficiary.PASSPORT', 'IC No. / Passport No.');
		$labels['Contact'] = _t('Beneficiary.CONTACT', 'Contact No.');
		$labels['Member.Username'] = _t('Beneficiary.USERNAME', 'Username');
		$labels['Member.Name'] = _t('Beneficiary.NAME', 'Name');
		$labels['Member.FirstName'] = _t('Beneficiary.FIRSTNAME', 'First Name');
		$labels['Member.Surname'] = _t('Beneficiary.SURNAME', 'Surname');
		
		return $labels;	
	}
	
    /**
     *
     * @return FieldList
     */
    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->removeByName('MemberID');

        $this->extend('updateCMSFields', $fields);

        return $fields;
    }

    /**
     * Custom BeneficiaryFormFields
     *
     * @return FieldList
     */
    public function getBeneficiaryFormFields() {
        $fields = parent::getFrontendFields();

		$fields->push(SecurityPinField::create('BeneficiarySecurityPin', _t('Beneficiary.SECURITY_PIN', 'Security Pin')));
        $fields->removeByName('MemberID');

        $this->extend('updateBeneficiaryFormFields', $fields);
        return $fields;
    }

	function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return $this->Member()->canView($member);
    }

    function canEdit($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return $this->Member()->canEdit($member);
    }

    function canDelete($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return $this->Member()->canDelete($member);
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return $this->Member()->canCreate($member);
    }
}
?>
