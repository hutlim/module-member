<?php
class Avatar extends Image {
    private static $singular_name = "Avatar";
    private static $plural_name = "Avatars";
    
    function canEdit($member = null) {
        return true;
    }
    
    function canView($member = null) {
        return true;
    }
    
    function canDelete($member = null) {
        return true;
    }

    function canCreate($member = null) {
        return true;
    }
}
?>