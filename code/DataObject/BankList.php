<?php
class BankList extends DropdownList implements PermissionProvider {
    private static $singular_name = "Bank List";
    private static $plural_name = "Bank Lists";

	private static $db = array(
		'Country' => 'Varchar(2)'
	);
	
	private static $default_sort = 'Country, Code, Sort';
	
    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($gender = GenderList::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $gender->Title;
        }
		return $code;
    }
	
	function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['Country'] = _t('BankList.COUNTRY', 'Country');
		
		return $labels;	
	}
	
	function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->replaceField('Country', CountryDropdownField::create('Country', $this->fieldLabel('Country')));

        return $fields;
    }
	
	function mapByCountry($country, $locale = null) {
    	$sourceClass = $this->class;
		if(!$locale) $locale = i18n::get_locale();
        return $sourceClass::get()->filter('Country', $country)->filter('Locale', $locale)->filter('Active', 1)->map('Code', 'Title');
    }
	
	function getDropdownFieldByCountry($country, $name, $title) {
        return DropdownField::create($name, $title)->setSource($this->mapByCountry($country)->toArray());
    }
}
?>
