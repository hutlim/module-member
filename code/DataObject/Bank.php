<?php
/**
 * @package member
 */
class Bank extends DataObject {
    private static $singular_name = "Bank";
    private static $plural_name = "Banks";
    
    private static $db = array(
    	'Country' => 'Varchar(2)',
        'BankName' => 'Varchar(100)',
        'BranchName' => 'Varchar(100)',
        'SwiftCode' => 'Varchar',
        'Currency' => 'Varchar',
        'Address' => 'Varchar(250)',
        'Suburb' => 'Varchar(64)',
        'State' => 'Varchar(64)',
        'Postal' => 'Varchar(10)',
        'AccountName' => 'Varchar',
        'AccountNumber' => 'Varchar',
        'OwnerContact' => 'Varchar',
        'OwnerEmail' => 'Varchar',
        'Confirmed' => 'Boolean'
    );

    private static $has_one = array('Member' => 'Member');

    private static $searchable_fields = array(
        'Member.Username',
        'Member.FirstName',
        'Member.Surname',
        'Country',
        'BankName',
        'BranchName',
        'Currency',
        'SwiftCode',
        'Address',
        'Suburb',
        'State',
        'Postal',
        'AccountName',
        'AccountNumber',
        'OwnerContact',
        'OwnerEmail',
        'Confirmed'
    );

    private static $summary_fields = array(
        'Member.Username',
        'Member.Name',
        'Country',
        'BankName',
        'BranchName',
        'Currency',
        'AccountName',
        'AccountNumber',
        'Confirmed.Nice'
    );

	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['BankName'] = _t('Bank.BANK_NAME', 'Bank Name');
		$labels['BranchName'] = _t('Bank.BRANCH_NAME', 'Branch Name');
		$labels['Currency'] = _t('Bank.CURRENCY', 'Currency');
		$labels['SwiftCode'] = _t('Bank.SWIFT_CODE', 'Swift Code / ABA');
		$labels['Address'] = _t('Bank.BANK_ADDRESS', 'Bank Address');
		$labels['Suburb'] = _t('Bank.BANK_SUBURB', 'Bank Suburb');
		$labels['State'] = _t('Bank.BANK_STATE', 'Bank State');
		$labels['Postal'] = _t('Bank.BANK_POSTAL', 'Bank Postal');
		$labels['Country'] = _t('Bank.BANK_COUNTRY', 'Bank Country');
		$labels['AccountName'] = _t('Bank.ACCOUNT_NAME', 'Account Name');
		$labels['AccountNumber'] = _t('Bank.ACCOUNT_NUMBER', 'Account No.');
		$labels['OwnerContact'] = _t('Bank.OWNER_CONTACT', 'Owner Contact');
		$labels['OwnerEmail'] = _t('Bank.OWNER_EMAIL', 'Owner Email');
		$labels['Confirmed'] = _t('Bank.CONFIRMED', 'Confirmed');
		$labels['Confirmed.Nice'] = _t('Bank.CONFIRMED', 'Confirmed');
		$labels['Member.Username'] = _t('Bank.USERNAME', 'Username');
		$labels['Member.Name'] = _t('Bank.NAME', 'Name');
		$labels['Member.FirstName'] = _t('Bank.FIRSTNAME', 'First Name');
		$labels['Member.Surname'] = _t('Bank.SURNAME', 'Surname');
		
		return $labels;	
	}

    /**
     *
     * @return FieldList
     */
    public function getCMSFields() {
        $fields = parent::getCMSFields();

		$fields->replaceField('Country', CountryDropdownField::create('Country', $this->fieldLabel('Country')));
		$fields->replaceField('BankName', BankNameField::create('BankName', $this->fieldLabel('BankName'))->setCountryField('Country'));
		$fields->replaceField('Currency', CurrencyDropdownField::create('Currency', $this->fieldLabel('Currency'))->setCountryField('Country'));
        $fields->removeByName('MemberID');

        return $fields;
    }
	
	/**
     *
     * @return FieldList
     */
    public function getFrontEndFields($params = null) {
        $fields = parent::getFrontEndFields($params);
		
		$fields->replaceField('Country', CountryDropdownField::create('Country', $this->fieldLabel('Country')));
		$fields->replaceField('BankName', BankNameField::create('BankName', $this->fieldLabel('BankName'))->setCountryField('Country'));
		$fields->replaceField('Currency', CurrencyDropdownField::create('Currency', $this->fieldLabel('Currency'))->setCountryField('Country'));
		$fields->removeByName('Confirmed');
        $fields->removeByName('MemberID');

        return $fields;
    }

    /**
     * Custom BankFormFields
     *
     * @return FieldList
     */
    public function getBankDetailsFormFields() {
        $fields = $this->getFrontendFields();

		$fields->push(SecurityPinField::create('BankSecurityPin', _t('Bank.SECURITY_PIN', 'Security Pin')));

        $this->extend('updateBankDetailsFormFields', $fields);
        return $fields;
    }

    function getBankInfoFields() {
        $field_list = array(
            'BankName',
            'BranchName',
            'Currency',
            'SwiftCode',
            'Address',
            'Suburb',
            'State',
            'Postal',
            'Country'
        );
        $dump = $this->getBankDetailsFormFields();
        $fields = FieldList::create();
        foreach($field_list as $field_name){
            if($field = $dump->fieldByName($field_name)){
                $fields->push($field);
            }
        }
        
		$fields->push(SecurityPinField::create('BankInfoSecurityPin', _t('Bank.SECURITY_PIN', 'Security Pin')));
		
        $this->extend('updateBankInfoFields', $fields);
        
        return $fields;
    }
    
    function getAccountInfoFields() {
        $field_list = array(
            'AccountName',
            'AccountNumber',
            'OwnerContact',
            'OwnerEmail'
        );
        $dump = $this->getBankDetailsFormFields();
        $fields = FieldList::create();
        foreach($field_list as $field_name){
            if($field = $dump->fieldByName($field_name)){
                $fields->push($field);
            }
        }
		
		$fields->push(SecurityPinField::create('AccountInfoSecurityPin', _t('Bank.SECURITY_PIN', 'Security Pin')));
        
        $this->extend('updateAccountInfoFields', $fields);
        
        return $fields;
    }

    function getCountryName() {
        $locale = $this->Member()->Locale ? $this->Member()->Locale : i18n::get_locale();
        return Zend_Locale::getTranslation($this->Country, "country", $locale);
    }

	function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return $this->Member()->canView($member);
    }

    function canEdit($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return $this->Member()->canEdit($member);
    }

    function canDelete($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return $this->Member()->canDelete($member);
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return $this->Member()->canCreate($member);
    }
}
?>
