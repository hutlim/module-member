<?php
class KYCStatusList extends DropdownList implements PermissionProvider {
    private static $singular_name = "KYC Status List";
    private static $plural_name = "KYC Status Lists";
    
    private static $default_records = array(
        array(
            'Code' => 'Pending',
            'Title' => 'Pending',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Process',
            'Title' => 'Process',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Rejected',
            'Title' => 'Rejected',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Approved',
            'Title' => 'Approved',
            'Sort' => 40,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Pending',
            'Title' => '有待审核',
            'Sort' => 50,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Process',
            'Title' => '正在审核',
            'Sort' => 60,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Rejected',
            'Title' => '已拒绝',
            'Sort' => 70,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Approved',
            'Title' => '已批准',
            'Sort' => 80,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($kyc = KYCStatusList::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $kyc->Title;
        }
		return $code;
    }
}
?>
