<?php
class GenderList extends DropdownList implements PermissionProvider {
    private static $singular_name = "Gender List";
    private static $plural_name = "Gender Lists";
    
    private static $default_records = array(
        array(
            'Code' => 'Male',
            'Title' => 'Male',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Female',
            'Title' => 'Female',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Male',
            'Title' => '男性',
            'Sort' => 30,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Female',
            'Title' => '女性',
            'Sort' => 40,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($gender = GenderList::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $gender->Title;
        }
		return $code;
    }
}
?>
