<?php

class Member_GroupExtension extends DataExtension {
    private static $db = array('IsDistributorGroup' => 'Boolean');

    function requireDefaultRecords() {
        if(!$distributorGroup = Group::get()->find('Code', 'free')) {
            Group::create()
            ->setField('Code', 'free')
            ->setField('IsDistributorGroup', 1)
            ->setField('Title', _t('Member_GroupExtension.FREE_MEMBER', 'Free Member'))
            ->setField('Sort', 10)
            ->write();
            DB::alteration_message('Distributor group created', 'created');
        }
		
		if(!$distributorGroup = Group::get()->find('Code', 'level1')) {
            Group::create()
            ->setField('Code', 'level1')
            ->setField('IsDistributorGroup', 1)
            ->setField('Title', 'Level 1 Member')
            ->setField('Sort', 20)
            ->write();
            DB::alteration_message('Distributor group created', 'created');
        }
		
		if(!$distributorGroup = Group::get()->find('Code', 'level2')) {
            Group::create()
            ->setField('Code', 'level2')
            ->setField('IsDistributorGroup', 1)
            ->setField('Title', 'Level 2 Member')
            ->setField('Sort', 30)
            ->write();
            DB::alteration_message('Distributor group created', 'created');
        }
		
		if(!$distributorGroup = Group::get()->find('Code', 'level3')) {
            Group::create()
            ->setField('Code', 'level3')
            ->setField('IsDistributorGroup', 1)
            ->setField('Title', 'Level 3 Member')
            ->setField('Sort', 40)
            ->write();
            DB::alteration_message('Distributor group created', 'created');
        }
		
		if(!$distributorGroup = Group::get()->find('Code', 'level4')) {
            Group::create()
            ->setField('Code', 'level4')
            ->setField('IsDistributorGroup', 1)
            ->setField('Title', 'Level 4 Member')
            ->setField('Sort', 50)
            ->write();
            DB::alteration_message('Distributor group created', 'created');
        }
		
		if(!$distributorGroup = Group::get()->find('Code', 'level5')) {
            Group::create()
            ->setField('Code', 'level5')
            ->setField('IsDistributorGroup', 1)
            ->setField('Title', 'Level 5 Member')
            ->setField('Sort', 60)
            ->write();
            DB::alteration_message('Distributor group created', 'created');
        }
    }

    function updateCMSFields(FieldList $fields) {
    	$fields->insertAfter(TextField::create('Code', $this->owner->fieldLabel('Code')), 'Title');
        $fields->insertAfter(CheckboxField::create('IsDistributorGroup', _t('Member_GroupExtension.IS_DISTRIBUTOR_GROUP', "This is the distributor group.")), 'ParentID');
    }
	
	function getTitleByLang(){
		if($this->owner->IsDistributorGroup){
			return _t(sprintf('DistributorGroup.%s', strtoupper($this->owner->Code)), $this->owner->Title);
		}
		
		return $this->owner->Title;
	}
}
