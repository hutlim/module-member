<?php

class Member_ControllerExtension extends Extension {
    private static $allowed_actions = array('MyLoginForm');
    
    function CurrentMember() {
        return Distributor::currentUser();
    }
    
    function getMemberArea(){
        $page = MemberAreaPage::get()->find('ClassName', 'MemberAreaPage');
        if($page && $page->hasExtension('Translatable') && $this->owner->Locale){
            $page = $page->getTranslation($this->owner->Locale);
        }
        return $page ? $page : null;
    }
    
    function getMemberAreaLink() {
        $page = $this->getMemberArea();
        return $page ? $page->Link() : null;
    }
    
    function getMemberDetails(){
        $page = MemberDetailsPage::get()->find('ClassName', 'MemberDetailsPage');
        if($page) return $page;
    }
    
    function getMemberDetailsLink() {
        $page = $this->getMemberDetails();
        if($page) return $page->Link();
    }
    
    function getBankDetails(){
        $page = BankDetailsPage::get()->find('ClassName', 'BankDetailsPage');
        if($page) return $page;
    }
    
    function getBankDetailsLink() {
        $page = $this->getBankDetails();
        if($page) return $page->Link();
    }
    
    function getChangePassword(){
        $page = ChangePasswordPage::get()->find('ClassName', 'ChangePasswordPage');
        if($page) return $page;
    }
    
    function getChangePasswordLink() {
        $page = $this->getChangePassword();
        if($page) return $page->Link();
    }
    
    function getKYCVerification(){
        $page = KYCVerificationPage::get()->find('ClassName', 'KYCVerificationPage');
        if($page) return $page;
    }
    
    function getKYCVerificationLink() {
        $page = $this->getKYCVerification();
        if($page) return $page->Link();
    }
    
    function getTermAndCondition(){
        $page = TermAndConditionPage::get()->find('ClassName', 'TermAndConditionPage');
        return $page ? $page : null;
    }
    
    function getTermAndConditionLink() {
        $page = $this->getTermAndCondition();
        return $page ? $page->Link() : null;
    }
    
    function MyLoginForm(){
        return MyLoginForm::create($this->owner, "MyLoginForm");
    }
}
